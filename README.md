# Welcome to project Lottus

## This project runs with Javascript technologies, such as React Native, Node JS and TypeScript.

- Create an AVD on Android Studio AVD manager named 'px3'.
- Place the custom bat scripts in some dir on your computer and map it to system PATH. It can be found on ./batch_files
- Setup the dev environment as instructions of React Native official site: https://reactnative.dev/docs/environment-setup
- Open 4 terminals
-- Terminal 1: run tsc -w
-- Terminal 2: run 'rn-r'
-- Terminal 3: run 'metro'
-- Terminal 4: run 'emu-d px3'

## Figma Mockups
Link to Figma mockups: http://bit.ly/lottus-mockups

## Description
Designed to more accurately play sorting games


## Command line instructions

You can also upload existing files from your computer using the instructions below.

#### Git global setup
```sh
git config --global user.name "Deivid Rodrigues"
git config --global user.email "deivid.likeanormaldude@hotmail.com"
```

#### Create a new repository
```sh
git clone https://gitlab.com/likeanormaldude/lottus.git
cd lottus
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

#### Push an existing folder
```sh
cd existing_folder
git init
git remote add origin https://gitlab.com/likeanormaldude/lottus.git
git add .
git commit -m "Initial commit"
git push -u origin master
```
#### Push an existing Git repository
```sh
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/likeanormaldude/lottus.git
git push -u origin --all
git push -u origin --tags
```




